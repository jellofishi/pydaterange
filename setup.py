import os
import sys

from setuptools import setup, find_packages

needs_pytest = {'pytest', 'test', 'ptr'}.intersection(sys.argv)
pytest_runner = ['pytest-runner == 4.2'] if needs_pytest else []


def get_version():
    return os.environ.get('PACKAGE_VERSION', '0.0.1')


setup(name='pydaterange',
      version=get_version(),
      description="",
      long_description="""\
""",
      classifiers=[],  # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='',
      author='',
      author_email='',
      url='',
      license='',
      packages=find_packages(include=['pydaterange']),
      include_package_data=True,
      zip_safe=False,
      setup_requires=[] + pytest_runner,
      install_requires=[
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
