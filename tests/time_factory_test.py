from datetime import timedelta
from time import sleep
import time

from hamcrest import assert_that, is_, instance_of, greater_than, none, not_

from pydaterange.date_range import DateRange
from pydaterange.factory import datetime_utc_from_milliseconds, datetime_to_milliseconds, beginning_of_time, \
    time_utc_now, now_utc_milliseconds, now_utc_seconds
from pydaterange.predicate import is_finite_non_empty_date_range
from pydaterange.tramsform import datetime_utc_from_seconds, datetime_utc_from_formatted, timedelta_to_milliseconds, \
    simplify_date_ranges, get_utc_beginning_of_day, date_ranges_to_timedelta, time_utc_from_seconds, \
    datetime_utc_to_formatted, get_containing_date_range


class TestFactory:

    def test_now_utc_milliseconds(self):
        result = now_utc_milliseconds()

        assert_that(result, instance_of(int))

        sleep(0.1)

        next_result = now_utc_milliseconds()

        assert_that(next_result, greater_than(result))

    def test_now_utc_seconds(self):
        result = now_utc_seconds()

        assert_that(result, instance_of(float))
        assert_that(result, greater_than(0))

        sleep(0.1)

        next_result = now_utc_seconds()

        assert_that(next_result, greater_than(result))

    def test_datetime(self):
        first_result_date = datetime_utc_from_seconds(1515070800)

        other_result_date = datetime_utc_from_seconds(1515078000)

        time_format = '%Y-%m-%d %H:%M:%S'

        assert_that(str(first_result_date.tzinfo), is_('UTC'))
        assert_that(str(other_result_date.tzinfo), is_('UTC'))

        # first_result_date_as_utc = first_result_date.replace(tzinfo=timezone('UTC'))

        assert_that(str(first_result_date.tzinfo), is_('UTC'))
        assert_that(first_result_date.strftime(time_format), is_('2018-01-04 13:00:00'))

        # assert_that(str(first_result_date_as_utc.tzinfo), is_('UTC'))
        # assert_that(first_result_date_as_utc.timestamp(), is_(1515070800))
        # assert_that(first_result_date_as_utc.strftime(time_format), is_('2018-01-04 13:00:00'))

        assert_that(str(other_result_date.tzinfo), is_('UTC'))
        assert_that(other_result_date.strftime(time_format), is_('2018-01-04 15:00:00'))

    def test_datetime_to_milliseconds(self):
        item = datetime_utc_from_seconds(1515070800)

        assert_that(datetime_to_milliseconds(item), is_(1515070800000))

    def test_datetime_from_milliseconds(self):
        item = datetime_utc_from_milliseconds(1515070800001)

        assert_that(datetime_to_milliseconds(item), is_(1515070800001))

    def test_beginning_of_time(self):
        item = beginning_of_time()

        assert_that(item, is_(datetime_utc_from_formatted('1970-01-01 00:00:00')))

    def test_timedelta_to_milliseconds(self):
        td = timedelta(seconds=5)

        assert_that(timedelta_to_milliseconds(td), is_(5000))

    def test_get_containing_date_range(self):
        date_ranges = [DateRange(datetime_utc_from_formatted('2019-01-01 12:09:43'),
                                 datetime_utc_from_formatted('2019-03-11 12:09:43')),
                       DateRange(datetime_utc_from_formatted('2018-01-01 17:09:43'),
                                 datetime_utc_from_formatted('2018-05-11 12:09:43'))]

        result = get_containing_date_range(date_ranges)

        assert_that(result, is_(DateRange(datetime_utc_from_formatted('2018-01-01 17:09:43'),
                                          datetime_utc_from_formatted('2019-03-11 12:09:43'), True, False)))

    def test_simplify_date_ranges(self):
        date_ranges = [DateRange(datetime_utc_from_formatted('2019-01-01 12:09:43'),  # intentionally unsorted
                                 datetime_utc_from_formatted('2019-03-11 12:09:43')),

                       DateRange(datetime_utc_from_formatted('2018-01-01 17:09:43'),
                                 datetime_utc_from_formatted('2018-05-11 12:09:43')),

                       DateRange(datetime_utc_from_formatted('2018-02-01 17:09:43'),
                                 datetime_utc_from_formatted('2018-06-11 12:09:43')),

                       DateRange(datetime_utc_from_formatted('2018-07-01 17:09:43'),
                                 datetime_utc_from_formatted('2018-08-11 12:09:43'))]

        expected_result = [DateRange(datetime_utc_from_formatted('2018-01-01 17:09:43'),
                                     datetime_utc_from_formatted('2018-06-11 12:09:43')),
                           DateRange(datetime_utc_from_formatted('2018-07-01 17:09:43'),
                                     datetime_utc_from_formatted('2018-08-11 12:09:43')),
                           DateRange(datetime_utc_from_formatted('2019-01-01 12:09:43'),
                                     datetime_utc_from_formatted('2019-03-11 12:09:43'))]

        result = simplify_date_ranges(date_ranges)

        assert_that(result, is_(expected_result))

    def test_is_finite_non_empty_date_range(self):
        date_range = DateRange(datetime_utc_from_formatted('2018-01-01 17:09:43'),
                               datetime_utc_from_formatted('2018-06-11 12:09:43'))

        assert_that(is_finite_non_empty_date_range(date_range), is_(True))

    def test_date_ranges_to_timedelta(self):
        date_ranges = [DateRange(datetime_utc_from_formatted('2018-01-01 17:09:43'),
                                 datetime_utc_from_formatted('2018-06-11 12:09:43')),
                       DateRange(datetime_utc_from_formatted('2018-07-01 17:09:43'),
                                 datetime_utc_from_formatted('2018-08-11 12:09:43')),
                       DateRange(datetime_utc_from_formatted('2019-01-01 12:09:43'),
                                 datetime_utc_from_formatted('2019-03-11 12:09:43'))]

        assert_that(date_ranges_to_timedelta(date_ranges), is_(timedelta(days=270, hours=13, minutes=59, seconds=57)))

    def test_get_utc_beginning_of_day(self):
        result = get_utc_beginning_of_day(datetime_utc_from_formatted('2018-01-01 17:09:43'))

        assert_that(result, is_(datetime_utc_from_formatted('2018-01-01 00:00:00')))

    def test_time_utc_from_seconds(self):
        item = time_utc_from_seconds(1234)

        assert_that(item, is_(time.gmtime(1234)))

    def test_time_utc_now(self):
        item = time_utc_now()

        assert_that(item, not_(none()))

    def test_datetime_utc_to_formatted(self):
        date_time = datetime_utc_from_formatted('2018-01-01 00:00:00')
        formatted = datetime_utc_to_formatted(date_time)

        assert_that(formatted, is_('2018-01-01 00:00:00'))