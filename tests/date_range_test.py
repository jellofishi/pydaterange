from copy import deepcopy
from datetime import datetime
from typing import List, Optional

import pytest
from hamcrest import assert_that, is_, not_, equal_to

from pydaterange.date_range import DateRange
from pydaterange.predicate import is_empty_date_range, date_range_both_edges_defined, date_range_contains_range, \
    date_range_starts_before
from pydaterange.tramsform import merge_date_ranges, datetime_utc_from_spec, invert_date_ranges, \
    get_date_range_intersection, filter_ordered_date_ranges_by_date_range


class TestUnitDateRange:

    def test_equality(self):
        range1 = DateRange(datetime(2018, 8, 11), datetime(2018, 9, 12))
        range2 = DateRange(datetime(2018, 8, 11), datetime(2018, 9, 12))

        different_range = DateRange(datetime(2018, 9, 11), datetime(2018, 9, 12))

        assert_that(range2, is_(range1))
        assert_that(range2, not_(is_(different_range)))

    def test_starts_before(self):
        range1 = DateRange(datetime(2018, 8, 11), datetime(2018, 9, 12))
        range2 = DateRange(datetime(2018, 8, 11), datetime(2018, 9, 12))

        assert_that(date_range_starts_before(range1, range2), is_(True))

    def test_merge_empty_date_ranges(self):
        merged_disjoint_ranges = merge_date_ranges(None, None)
        assert_that(merged_disjoint_ranges, equal_to(None))

        merged_disjoint_ranges = merge_date_ranges([], [])
        assert_that(merged_disjoint_ranges, equal_to(None))

        merged_disjoint_ranges = merge_date_ranges(None, [])
        assert_that(merged_disjoint_ranges, equal_to(None))

        merged_disjoint_ranges = merge_date_ranges([], None)
        assert_that(merged_disjoint_ranges, equal_to(None))

        range1 = DateRange(datetime(2018, 8, 11), datetime(2018, 9, 12))
        list1 = [range1]

        merged_disjoint_ranges = merge_date_ranges(list1, None)
        assert_that(merged_disjoint_ranges, equal_to(None))

        merged_disjoint_ranges = merge_date_ranges(None, list1)
        assert_that(merged_disjoint_ranges, equal_to(None))

        merged_disjoint_ranges = merge_date_ranges(list1, [])
        assert_that(merged_disjoint_ranges, equal_to(None))

        merged_disjoint_ranges = merge_date_ranges([], list1)
        assert_that(merged_disjoint_ranges, equal_to(None))

    def test_merge_date_ranges(self):
        range1 = DateRange(datetime(2018, 8, 11), datetime(2018, 9, 12))
        list1 = [range1]

        range2 = DateRange(datetime(2018, 9, 13), datetime(2018, 10, 13))
        list2 = [range2]

        merged_disjoint_ranges = merge_date_ranges(list1, list2)
        assert_that(merged_disjoint_ranges, equal_to([range1, range2]))

        range3 = DateRange(datetime(2018, 8, 13), datetime(2018, 10, 13))
        list3 = [range3]
        merged_overlaping_ranges = merge_date_ranges(list1, list3)
        merged_range = deepcopy(range3)
        merged_range.start = range1.start
        assert_that(merged_overlaping_ranges, equal_to([merged_range]))

        merged_overlapping_and_disjoint_ranges = merge_date_ranges(merged_disjoint_ranges, [merged_range])
        assert_that(merged_overlapping_and_disjoint_ranges, equal_to([merged_range]))

        range4 = DateRange(datetime(2018, 8, 10), datetime(2018, 8, 15))

        merged_range = DateRange(range4.start, range1.end)

        merged_overlaping_and_disjoint_ranges2 = merge_date_ranges([range1, range2], [range4])
        assert_that(merged_overlaping_and_disjoint_ranges2, equal_to([merged_range, range2]))

    @pytest.mark.parametrize('date_range, expected_result', [
        (DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 4)), False),
        (DateRange(), True),
        (None, True),
    ])
    def test_is_empty_date_range(self, date_range: DateRange, expected_result: bool):
        assert_that(is_empty_date_range(date_range), is_(expected_result))

    @pytest.mark.parametrize('date_range, expected_result', [
        (DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 4)), True),
        (DateRange(), False)
    ])
    def test_date_range_both_edges_defined(self, date_range: DateRange, expected_result: bool):
        assert_that(date_range_both_edges_defined(date_range), is_(expected_result))

    @pytest.mark.parametrize('date_ranges, enclosing_date_range, expected_date_ranges', [
        ([DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 4)),
          DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 4)),
          DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 4)),
          DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 4))],
         DateRange(datetime_utc_from_spec(2019, 12, 1), datetime_utc_from_spec(2020, 2, 4)),
         [DateRange(datetime_utc_from_spec(2019, 12, 1), datetime_utc_from_spec(2020, 1, 1), include_end=True),
          DateRange(datetime_utc_from_spec(2020, 1, 4), datetime_utc_from_spec(2020, 2, 4), include_end=True)]
         ),

        ([DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 4)),
          DateRange(datetime_utc_from_spec(2020, 1, 6), datetime_utc_from_spec(2020, 1, 8)),
          DateRange(datetime_utc_from_spec(2020, 1, 10), datetime_utc_from_spec(2020, 1, 12)),
          DateRange(datetime_utc_from_spec(2020, 1, 12), datetime_utc_from_spec(2020, 1, 14))],
         DateRange(datetime_utc_from_spec(2019, 12, 1), datetime_utc_from_spec(2020, 2, 4)),
         [DateRange(datetime_utc_from_spec(2019, 12, 1), datetime_utc_from_spec(2020, 1, 1), include_end=True),
          DateRange(datetime_utc_from_spec(2020, 1, 4), datetime_utc_from_spec(2020, 1, 6), include_end=True),
          DateRange(datetime_utc_from_spec(2020, 1, 8), datetime_utc_from_spec(2020, 1, 10), include_end=True),
          DateRange(datetime_utc_from_spec(2020, 1, 14), datetime_utc_from_spec(2020, 2, 4), include_end=True),
          ]
         ),

        ([],
         DateRange(datetime_utc_from_spec(2019, 12, 1), datetime_utc_from_spec(2020, 2, 4)),
         [DateRange(datetime_utc_from_spec(2019, 12, 1), datetime_utc_from_spec(2020, 2, 4), include_end=True)]
         ),

        ([DateRange(datetime_utc_from_spec(2020, 11, 3, 8), datetime_utc_from_spec(2020, 11, 4, 8)),
          DateRange(datetime_utc_from_spec(2020, 9, 25, 20), datetime_utc_from_spec(2020, 9, 26, 20))],

         DateRange(datetime_utc_from_spec(2019, 11, 18, 9, 59), datetime_utc_from_spec(2020, 11, 3, 9, 59)),

         [DateRange(datetime_utc_from_spec(2019, 11, 18, 9, 59), datetime_utc_from_spec(2020, 9, 25, 20),
                    include_end=True),
          DateRange(datetime_utc_from_spec(2020, 9, 26, 20), datetime_utc_from_spec(2020, 11, 3, 8), include_end=True)
          ]
         ),

        ([],

         None,

         []
         ),
    ])
    def test_invert_date_ranges(self,
                                date_ranges: List[DateRange],
                                enclosing_date_range: DateRange,
                                expected_date_ranges: List[DateRange]):
        inverted_date_ranges = invert_date_ranges(date_ranges,
                                                  enclosing_date_range=enclosing_date_range)

        assert_that(inverted_date_ranges, is_(expected_date_ranges))

    @pytest.mark.parametrize('date_range, other_date_range, expected_intersection', [
        (DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 4)),
         DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 4)),
         DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 4))
         ),
        (DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 2)),
         DateRange(datetime_utc_from_spec(2020, 1, 3), datetime_utc_from_spec(2020, 1, 4)),
         None
         ),
        (DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 4)),
         DateRange(datetime_utc_from_spec(2020, 1, 2), datetime_utc_from_spec(2020, 1, 3)),
         DateRange(datetime_utc_from_spec(2020, 1, 2), datetime_utc_from_spec(2020, 1, 3))
         ),
        (DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 3)),
         DateRange(datetime_utc_from_spec(2020, 1, 2), datetime_utc_from_spec(2020, 1, 4)),
         DateRange(datetime_utc_from_spec(2020, 1, 2), datetime_utc_from_spec(2020, 1, 3))
         ),
    ])
    def test_get_date_range_intersection(self,
                                         date_range: DateRange,
                                         other_date_range: DateRange,
                                         expected_intersection: Optional[DateRange]):
        intersection = get_date_range_intersection(date_range, other_date_range)

        assert_that(intersection, is_(expected_intersection))

    @pytest.mark.parametrize('date_ranges, filter_date_range, expected_date_ranges', [
        ([
             DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 2)),
             DateRange(datetime_utc_from_spec(2020, 1, 3), datetime_utc_from_spec(2020, 1, 4))
         ],
         DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 4)),
         [
             DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 2)),
             DateRange(datetime_utc_from_spec(2020, 1, 3), datetime_utc_from_spec(2020, 1, 4))
         ]),
        ([DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 2))],
         DateRange(datetime_utc_from_spec(2020, 1, 3), datetime_utc_from_spec(2020, 1, 4)),
         []
         ),
        ([
             DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 2)),
             DateRange(datetime_utc_from_spec(2020, 1, 3), datetime_utc_from_spec(2020, 1, 4))
         ],
         DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 3, 12)),
         [
             DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 2)),
             DateRange(datetime_utc_from_spec(2020, 1, 3), datetime_utc_from_spec(2020, 1, 3, 12))
         ]),
        ([
             DateRange(datetime_utc_from_spec(2020, 1, 1), datetime_utc_from_spec(2020, 1, 2)),
             DateRange(datetime_utc_from_spec(2020, 1, 3), datetime_utc_from_spec(2020, 1, 4))
         ],
         DateRange(datetime_utc_from_spec(2020, 1, 1, 12), datetime_utc_from_spec(2020, 1, 3, 12)),
         [
             DateRange(datetime_utc_from_spec(2020, 1, 1, 12), datetime_utc_from_spec(2020, 1, 2)),
             DateRange(datetime_utc_from_spec(2020, 1, 3), datetime_utc_from_spec(2020, 1, 3, 12))
         ]),
    ])
    def test_filter_ordered_date_ranges_by_date_range(self,
                                                      date_ranges: List[DateRange],
                                                      filter_date_range: DateRange,
                                                      expected_date_ranges: List[DateRange]):
        filtered_date_ranges = filter_ordered_date_ranges_by_date_range(date_ranges, filter_date_range)

        assert_that(filtered_date_ranges, is_(expected_date_ranges))

    def test_date_range_contains_range(self):
        result = date_range_contains_range(
            DateRange(datetime_utc_from_spec(2020, 1, 2, 12), datetime_utc_from_spec(2020, 1, 4)),
            DateRange(datetime_utc_from_spec(2020, 1, 3), datetime_utc_from_spec(2020, 1, 3, 12))
        )

        assert_that(result, is_(True))
