from typing import List, Collection

from pydaterange.date_range import DateRange

__all__ = [
    'sorted_date_ranges'
]


def sorted_date_ranges(date_ranges: Collection[DateRange]) -> List[DateRange]:
    return sorted(date_ranges, key=lambda date_range: date_range.start)
