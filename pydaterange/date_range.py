from dataclasses import dataclass
from datetime import datetime
from typing import Optional

__all__ = [
    'DateRange'
]


@dataclass
class DateRange:
    start: Optional[datetime] = None
    end: Optional[datetime] = None
    include_start: bool = True
    include_end: bool = False

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, DateRange):
            return False

        return self.__dict__ == other.__dict__

    def __hash__(self) -> int:
        return hash((self.start, self.end, self.include_start, self.include_end))

    def __repr__(self):
        return '{}(start={},end={},include_start={},include_end={})'.format(
            self.__class__.__name__,
            repr(self.start),
            repr(self.end),
            self.include_start,
            self.include_end
        )

    def __str__(self):
        return '{}(start={},end={},include_start={},include_end={})'.format(
            self.__class__.__name__,
            repr(self.start),
            repr(self.end),
            self.include_start,
            self.include_end
        )
