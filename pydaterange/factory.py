import time
from datetime import datetime, timezone

__all__ = [
    'beginning_of_time',
    'get_utc_timezone',
    'time_utc_now',
    'datetime_utc_now',
    'datetime_utc_from_milliseconds',
    'datetime_to_milliseconds',
    'now_utc_seconds',
    'now_utc_milliseconds'
]


def datetime_utc_from_milliseconds(milliseconds: int) -> datetime:
    return datetime.fromtimestamp(milliseconds / 1000, tz=get_utc_timezone())


def beginning_of_time() -> datetime:
    return datetime_utc_from_milliseconds(0)


def time_utc_now() -> time:
    return time.gmtime()


def datetime_utc_now() -> datetime:
    return datetime.now(tz=get_utc_timezone())


def get_utc_timezone():
    return timezone.utc


def now_utc_seconds() -> float:
    return datetime_utc_now().timestamp()


def now_utc_milliseconds() -> int:
    return datetime_to_milliseconds(datetime_utc_now())


def datetime_to_milliseconds(item: datetime) -> int:
    return int(item.timestamp() * 1000)
