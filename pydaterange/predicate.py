__all__ = [
    'is_empty_date_range',
    'is_finite_non_empty_date_range',
    'is_finite_date_range',
    'date_range_both_edges_defined',
    'date_range_contains_range',
    'date_range_contains_datetime',
    'date_range_starts_before',
    'is_date_range_overlap',
    'is_empty'
]

from datetime import datetime
from typing import Collection

from pydaterange.base import sorted_date_ranges
from pydaterange.date_range import DateRange


def is_finite_non_empty_date_range(date_range: DateRange) -> bool:
    return not is_empty_date_range(date_range) and date_range.end is not None


def is_empty_date_range(date_range: DateRange) -> bool:
    if date_range is None:
        return True

    if date_range.start is None:
        return True

    if date_range.start is not None and date_range.end is None:
        return False

    return date_range.start > date_range.end or \
           (date_range.start == date_range.end and
            (not date_range.include_start or not date_range.include_end))


def is_finite_date_range(date_range: DateRange) -> bool:
    return date_range.start is not None and date_range.end is not None


def date_range_both_edges_defined(date_range: DateRange) -> bool:
    return date_range.start is not None or date_range.end is not None


def date_range_contains_range(date_range: DateRange, other: DateRange) -> bool:
    return date_range.start <= other.start and date_range.end >= other.end


def date_range_contains_datetime(date_range: DateRange, date_time: datetime) -> bool:
    return date_range.start <= date_time <= date_range.end


def date_range_starts_before(date_range: DateRange, other: DateRange) -> bool:
    return date_range.start <= other.start


def is_date_range_overlap(date_range_1: DateRange, date_range_2: DateRange) -> bool:
    first_date_range, second_date_range = sorted_date_ranges([date_range_1, date_range_2])

    return first_date_range.start <= second_date_range.start <= first_date_range.end


def is_empty(collection: Collection) -> bool:
    return len(collection) == 0
