import time
from copy import deepcopy
from datetime import datetime, timedelta
from itertools import chain
from typing import List, Union, Optional, Collection

from pydaterange.base import sorted_date_ranges
from pydaterange.date_range import DateRange
from pydaterange.factory import get_utc_timezone, datetime_utc_from_milliseconds
from pydaterange.predicate import is_finite_date_range, is_empty, date_range_contains_datetime, is_date_range_overlap

__all__ = [
    'datetime_utc_from_seconds',
    'time_utc_from_seconds',
    'datetime_utc_from_formatted',
    'datetime_utc_from_spec',
    'timedelta_to_milliseconds',
    'datetime_utc_to_formatted',
    'get_utc_beginning_of_day',
    'get_containing_date_range',
    'simplify_date_ranges',
    'merge_date_ranges',
    'invert_date_ranges',
    'get_date_range_intersection',
    'filter_ordered_date_ranges_by_date_range',
    'date_ranges_to_timedelta',
    'date_range_to_timedelta',
]


def time_utc_from_seconds(seconds: int) -> time:
    return time.gmtime(seconds)


def datetime_utc_from_seconds(seconds: int) -> datetime:
    return datetime.fromtimestamp(seconds, tz=get_utc_timezone())


def datetime_utc_from_spec(year: int, month: int, day: int, hour: int = 0, minute: int = 0,
                           second: int = 0) -> datetime:
    return datetime(year, month, day, hour, minute, second).replace(tzinfo=get_utc_timezone())


def datetime_utc_from_formatted(serialized_date: Optional[Union[str, int]],
                                date_format: str = '%Y-%m-%d %H:%M:%S') -> Optional[datetime]:
    if serialized_date is None:
        return None

    if isinstance(serialized_date, int):
        return datetime_utc_from_milliseconds(serialized_date)

    return datetime.strptime(serialized_date, date_format).replace(tzinfo=get_utc_timezone())


def datetime_utc_to_formatted(date: datetime, date_format: str = '%Y-%m-%d %H:%M:%S') -> str:
    return date.strftime(date_format)


def timedelta_to_milliseconds(td: timedelta) -> int:
    return int(td / timedelta(milliseconds=1))


def simplify_date_ranges(date_ranges: List[DateRange]) -> List[DateRange]:
    if len(date_ranges) == 0:
        return []

    date_ranges = sorted_date_ranges(date_ranges)
    simplified_date_ranges = [date_ranges[0]]

    for date_range in date_ranges[1:]:
        last_date_range = simplified_date_ranges[-1]

        if is_date_range_overlap(last_date_range, date_range):
            simplified_date_ranges[-1] = get_containing_date_range([last_date_range, date_range])
        else:
            simplified_date_ranges.append(date_range)

    return simplified_date_ranges


def date_ranges_to_timedelta(date_ranges: List[DateRange], edge_size: timedelta = timedelta(seconds=1)) -> timedelta:
    td = sum([date_range_to_timedelta(date_range, edge_size) for date_range in date_ranges], start=timedelta())
    return td


def date_range_to_timedelta(date_range: DateRange, edge_size: timedelta = timedelta(seconds=1)) -> timedelta:
    if not is_finite_date_range(date_range):
        return timedelta()

    td = date_range.end - date_range.start

    if not date_range.include_end:
        td = td - edge_size

    if not date_range.include_start:
        td = td - edge_size

    return td


def get_containing_date_range(date_ranges: Collection[DateRange]) -> DateRange:
    minimum_date_range = min(*[date_range for date_range in date_ranges], key=lambda dr: dr.start)
    maximum_date_range = max(*[date_range for date_range in date_ranges], key=lambda dr: dr.end)

    return DateRange(minimum_date_range.start, maximum_date_range.end,
                     minimum_date_range.include_start, maximum_date_range.include_end)


def get_date_range_intersection(date_range: DateRange, other_date_range: DateRange) -> Optional[DateRange]:
    if not is_date_range_overlap(date_range, other_date_range):
        return None
    date_ranges = [date_range, other_date_range]

    minimum_date_range = max(*[date_range for date_range in date_ranges], key=lambda dr: dr.start)
    maximum_date_range = min(*[date_range for date_range in date_ranges], key=lambda dr: dr.end)

    return DateRange(minimum_date_range.start, maximum_date_range.end,
                     minimum_date_range.include_start, maximum_date_range.include_end)


def get_utc_beginning_of_day(date_time: datetime) -> datetime:
    return date_time.replace(hour=0, minute=0, second=0, microsecond=0, tzinfo=get_utc_timezone())


def filter_ordered_date_ranges_by_date_range(date_ranges: [DateRange],
                                             date_range_filter: DateRange) -> List[DateRange]:
    result_date_ranges = []
    for date_range in date_ranges:
        intersection = get_date_range_intersection(date_range, date_range_filter)
        if intersection is not None:
            result_date_ranges.append(intersection)

    return result_date_ranges


def invert_date_ranges(date_ranges: List[DateRange],
                       enclosing_date_range: Optional[DateRange] = None) -> List[DateRange]:
    simplified_date_ranges = sorted_date_ranges(simplify_date_ranges(date_ranges))

    if is_empty(simplified_date_ranges):
        if enclosing_date_range is not None:
            return [DateRange(enclosing_date_range.start, enclosing_date_range.end, include_end=True)]
        else:
            return []

    if enclosing_date_range is not None:
        contained_date_ranges = [date_range for date_range in simplified_date_ranges
                                 if is_date_range_overlap(date_range, enclosing_date_range)]

        date_times = list(
            chain(*[(date_range.start, date_range.end) for date_range in contained_date_ranges]))

        if is_empty(contained_date_ranges):
            return []

        if date_range_contains_datetime(contained_date_ranges[0], enclosing_date_range.start):
            date_times = date_times[1:]
        else:
            date_times = [enclosing_date_range.start] + date_times

        if date_range_contains_datetime(contained_date_ranges[-1], enclosing_date_range.end):
            date_times = date_times[:-1]
        else:
            date_times = date_times + [enclosing_date_range.end]
    else:
        date_times = list(
            chain(*[(date_range.start, date_range.end) for date_range in simplified_date_ranges]))

        date_times = date_times[1:-1]

    if is_empty(date_times):
        inverted_date_ranges = []
    else:
        inverted_date_ranges = [
            DateRange(date_times[date_time_index], date_times[date_time_index + 1], include_end=True)
            for date_time_index in range(0, len(date_times), 2)
        ]

    return inverted_date_ranges


def merge_date_ranges(date_ranges1: Optional[List[DateRange]],
                      date_ranges2: Optional[List[DateRange]]) -> Optional[List[DateRange]]:
    if not date_ranges1 or not date_ranges2:
        return None

    # todo - deal with include start and include end
    all_date_ranges = []
    all_date_ranges.extend(date_ranges1)
    all_date_ranges.extend(date_ranges2)
    all_date_ranges.sort(key=lambda date_range: date_range.start)

    merged_date_ranges = [deepcopy(all_date_ranges[0])]

    previous_date_range = merged_date_ranges[0]
    for i in range(1, len(all_date_ranges)):
        # Current date range starts after the last date_range
        current_date_range = all_date_ranges[i]
        if current_date_range.start > previous_date_range.end:
            merged_date_ranges.append(deepcopy(current_date_range))
        # Current date range overlaps with last date_range
        elif current_date_range.end > previous_date_range.end:
            merged_date_ranges[i - 1].end = current_date_range.end

    return merged_date_ranges

