# pyDateRange

Date range data class.
Manipulation methods for dates, time-delta and date-ranges.

## API

### Timezone
* get_utc_timezone

### Serialize / Deserialize
* datetime_utc_from_seconds
* datetime_utc_from_milliseconds
* datetime_to_milliseconds
* time_utc_from_seconds
* datetime_utc_from_formatted
* datetime_utc_from_spec
* timedelta_to_milliseconds
* datetime_utc_to_formatted

### Now
* now_utc_seconds
* now_utc_milliseconds
* time_utc_now
* datetime_utc_now

### Beginning of time
* get_utc_beginning_of_day
* beginning_of_time

### Date Range
* get_containing_date_range
* simplify_date_ranges
* merge_date_ranges
* invert_date_ranges
* get_date_range_intersection
* filter_ordered_date_ranges_by_date_range
* date_range_starts_before
* is_empty_date_range
* is_finite_non_empty_date_range
* date_ranges_to_timedelta
* date_range_to_timedelta
* date_range_both_edges_defined
* date_range_contains_range
